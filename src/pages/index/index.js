import debounce from "lodash-es/debounce"

$(() => {
  const $container = $('[data-roadmap-container]')
  const $roadmapBlocks = $('[data-roadmap-block]')
  const $roadmapBlocksColored = $('[data-roadmap-block=colored]')
  const transitionTimingFunc = 'linear'
  const transitionTime = 0.66
  const $animateObjects = []

  const setPropSideLines = (el, size, side) => {
    const $containerOffsetLeft = $container.offset().left
    const $containerPaddingLeft = parseInt($container.css('padding-left'))

    el
      .addClass(`_line-${side}-${size}`)
      .find(`[data-roadmap-line-${side}]`)
      .attr(`data-roadmap-line-${side}`, 'animate')
      .css('width', `${$containerOffsetLeft + $containerPaddingLeft}px`)
  }

  const preparationElements = () => {
    const $windowWidth = $(window).width()
    const countColMd = 3
    const countColLg = 4

    $.each($roadmapBlocks, (i, el) => {
      const index = (i + 1)

      if (index === 1) $(el).addClass('_block-first')
      if (index === $roadmapBlocks.length) $(el).addClass('_block-last')

      if ($windowWidth >= 768 && $windowWidth < 1024) {
        if (index % countColMd === 1 && !$(el).hasClass('_block-first')) {
          setPropSideLines($(el), 'md', 'left')
        }
        if (index % countColMd === 0 && !$(el).hasClass('_block-last')) {
          setPropSideLines($(el), 'md', 'right')
        }
      }

      if ($windowWidth >= 1024) {
        if (index % countColLg === 1 && !$(el).hasClass('_block-first')) {
          setPropSideLines($(el), 'lg', 'left')
        }
        if (index % countColLg === 0 && !$(el).hasClass('_block-last')) {
          setPropSideLines($(el), 'lg', 'right')
        }
      }
    })

    $.each($roadmapBlocksColored, (i, el) => {
      $animateObjects
        .push(
          ...$(el).find('[data-roadmap-line-left=animate]'),
          $(el).find('[data-roadmap-title], [data-roadmap-circle], [data-roadmap-line-middle]'),
          ...$(el).find('[data-roadmap-line-right=animate]')
        )
    })
  }

  const init = () => {
    preparationElements()

    $.each($animateObjects, (i, el) => {
      $(el)
        .attr('data-roadmap-animate', '')
        .css({
          'transition-timing-function': transitionTimingFunc,
          'transition-delay': `${i * (transitionTime - 0.06)}s`,
          'transition-duration': `${transitionTime}s`,
        })
    })
  }

  const resize = () => {
    preparationElements()

    $.each($animateObjects, (i, el) => {
      $(el)
        .css({'transition-delay': '', 'transition': 'none'})
        .attr('data-roadmap-animate', '')
    })
  }

  init()

  $(window).on("resize", debounce(resize, 300, {trailing: true, leading: false}))
})
