/* eslint-disable */
const path = require("path")
const gulp = require("gulp")
const plumber = require("gulp-plumber")

const browsers = ["last 2 versions", "not IE 10"]
const customMedia = {
  "--screen--md": "only screen and (min-width: 768px)",
  "--screen--lg": "only screen and (min-width: 1024px)",
  "--screen--xl": "only screen and (min-width: 1366px)",
  "--screen--mobile-max": "only screen and (max-width: 767px)"
}
const customVariables = {
  "--color--dark-blue": "#272a75",
  "--color--purple": "#484bfc",
  "--color--light-purple": "#e2e0ff",
  "--color--aqua": "#57d9ad"
}

const postcss = [
  require("postcss-import")({
    path: [path.resolve(__dirname, "src/assets")],
  }),
  require("postcss-custom-media")({
    extensions: customMedia,
  }),
  require("postcss-custom-properties")({
    preserve: "computed",
    variables: customVariables,
  }),
  require("postcss-inline-svg")({
    path: path.resolve(__dirname, "src/public/static"),
  }),
  require("postcss-for")(),
  require("postcss-nested")(),
  require("autoprefixer")({
    browsers
  }),
  require("postcss-prettify"),
]

const webpack = {
  devtool: "source-map",
  output: {
    filename: "bundle.js"
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: "babel-loader",
      options: {
        presets: [
          ["env", {
            targets: {
              browsers
            }
          }]
        ]
      },
    }, ],
  },
}

const errors = (error) => {
  require("gulp-notify")({
    title: "¯\\_(ツ)_/¯",
    subtitle: error.plugin,
    message: error.message,
  }).write(error)
}

gulp
  .task("build:pages", () => {
    return gulp
      .src(path.resolve(__dirname, "src/pages/**/*.page.pug"))
      .pipe(plumber(errors))
      .pipe(require("gulp-pug")({
        pretty: true,
        basedir: (path.resolve(__dirname, "src/pages"))
      }))
      .pipe(require("gulp-rename")((file) => {
        file.dirname = ''
        file.basename = file.basename.replace(/.page/g, '');
      }))
      .pipe(gulp.dest(path.resolve(__dirname, "build")))
  })
  .task("build:stylesheets", () => {
    return gulp
      .src(path.resolve(__dirname, "src/pages/**/*.pcss"))
      .pipe(plumber(errors))
      .pipe(require("gulp-sourcemaps").init())
      .pipe(require("gulp-postcss")(postcss))
      .pipe(require("gulp-concat")("styles.css"))
      .pipe(require("gulp-sourcemaps").write("."))
      .pipe(gulp.dest(path.resolve(__dirname, "build")))
  })
  .task("build:javascripts", () => {
    return gulp
      .src(path.resolve(__dirname, "src/pages/**/*.js"))
      .pipe(plumber(errors))
      .pipe(require("webpack-stream")(webpack))
      .pipe(gulp.dest(path.resolve(__dirname, "build")))
  })
  .task("copy:static", () => {
    return gulp
      .src(path.resolve(__dirname, "src/public/**/*"))
      .pipe(gulp.dest(path.resolve(__dirname, "build")))
  })

gulp.task("build", ["build:pages", "build:stylesheets", "build:javascripts", "copy:static"])
gulp.task("start", ["build"], () => {
  require("browser-sync")
    .create()
    .init({
      files: "build/*.*",
      server: {
        baseDir: "build",
        directory: true
      },
      port: 55995,
      open: false,
      notify: false,
    })

  gulp.watch(path.resolve(__dirname, "src/**/*.pug"), ["build:pages"])
  gulp.watch(path.resolve(__dirname, "src/pages/**/*.pcss"), ["build:stylesheets"])
  gulp.watch(path.resolve(__dirname, "src/pages/**/*.js"), ["build:javascripts"])
})
